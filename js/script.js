//"use strict";
var CC = {
    
    debug : false,
    
    log : function(msg) {
        if (debug) {
            console.log(msg);
        }
    },
    
    // A jQuery-style selector without the cruft of jQuery
    $ : function(el) {
        if (el.substr(0, 1) === "#") {
            return document.getElementById(el.substr(1));
        }
        else if (el.substr(0, 1) === ".") {
            return document.getElementsByClassName(el.substr(1));
        }
        else {
            return document.getElementsByTagName(el);
        }
    },
    
    supports_history_api : function() {
        return !!(window.history && history.pushState);
    },
    
    // A couple of flags to control the app behavior
    uri : "/learning",
    withStars : false,
    
    stars : {
        "white": "☆",
        "black": "★"
    },
    
    idx : lunr(function() {
        this.field("id"),
        this.field("title", { boost: 10 }),
        this.field("tags"),
        this.field("body")
    }),
    
    buildSearchIndex : function() {
        for (var i=0, j=CC.lessons.length; i<j; i++) {
            CC.idx.add({
                id: i,
                title: CC.lessons[i].title,
                tags: CC.lessons[i].tags,
                body: CC.lessons[i].body
            });
        }
    },
    
    // Given an array of lessons (see CC.lessons for format), findTags()
    // returns a flattened, unique, sorted array of tags with onclick 
    // links that can return lessonsByTags()
    findTags : function(q) {
        if (typeof(q) !== "undefined") {
            return q.map(function(e) {
                return e.tags;
            })
            
            // flatten the array of tags
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce#Flatten_an_array_of_arrays
            .reduce(function(a, b) {
                return a.concat(b);
            }, [])
            
            // make the elements unique
            // http://stackoverflow.com/questions/1960473/unique-values-in-an-array
            .filter(function(value, index, self) { 
                return self.indexOf(value) === index;
            })
            
            // sort it all
            .sort()
            
            // add onclick
            .map(function(tag) {
                
                // tag_val is used in links so that "+" is used instead of 
                // space in any tags with spaces. For example, "data format"
                // becomes "data+format"
                var tag_val = tag.replace(/ /g, "+");
                
                //var minus_link = "<div class='ltag'><a href='#' onclick='CC.removeTag(\"" + tag + "\"); return false;'>-</a></div>";
                //var plus_link  = "<div class='rtag'><a href='#' onclick='CC.addTag(\"" + tag + "\"); return false;'>+</a></div>";
                var minus_link = "";
                var plus_link = "";
                
                var href = CC.uri + "?tags=" + tag_val;
                var tag_link   = "<div class='mtag'><a href='" + href + "' onclick='CC.lessonsByTags(\"" + tag + "\"); history.pushState(null, \"" + href + "\", \"" + href + "\"); return false;'>" + tag + "</a></div>";
                return minus_link + tag_link + plus_link;
            });
        }
    },
    
    // Extract tags from the URI location string's search part
    tagsFromLocation : function() {

        // given search = '?tags=foo;bar;baz&mood=blah&income=high'
        // return 'tags=foo;bar;baz&mood=blah&income=high'
        var a = document.location.search.substr(1);
        
        if (a === "") {
            return a;
        }
        else {
            // then [ 'tags=foo;bar;baz', 'mood=blah', 'income=high' ]
            return a.split("&")
            
                // then return 'tags=foo;bar;baz'
                .filter(function(el) { if (el.indexOf("tags=") != -1) { return el; }})[0]
            
                // then return foo;bar;baz
                .substr(5); 
        } 
    },
    
    addTag : function(tag) {
        var curr_tags = CC.tagsFromLocation();
        
        var new_tags = curr_tags === "" ? tag : curr_tags + ";" + tag;

        var href = CC.uri + "?tags=" + new_tags;
        
        history.pushState(null, href, href);
        CC.lessonsByTags(new_tags);
    },
    
    removeTag : function(tag) {
        var curr_tags = CC.tagsFromLocation();
        
        // then return ['foo', 'bar', 'baz']
        var new_tags = curr_tags.split(/;/)
        
            // return ['foo', 'baz'] given 'tag' === 'bar'
            .filter(function(el) { if (el !== tag) { return el; }})
            
            // return 'foo;baz'
            .join(';');
        
        var href = CC.uri;
        
        //console.log("new tags: " + new_tags);
        if (new_tags !== "") {
            href = CC.uri + "?tags=" + new_tags;
            history.pushState(null, href, href);
            CC.lessonsByTags(new_tags);
        }
        else {
            history.pushState(null, href, href);
            CC.write(CC.lessons);
        }
        
        
    },
    
    starrer : function(entry, votes) {        
        var i,
            str = "";
            
            // This is where we calculate the weighted stars.
            
            /*
            Film rankings (IMDb Top 250)
            
            https://en.wikipedia.org/wiki/Internet_Movie_Database#User_ratings_of_films
            
            The IMDb Top 250 list is a listing of the top rated 250 films of all-time, based on ratings by the registered users of the website using the methods described. Currently, The Shawshank Redemption is #1 on the list.[24] The 'top 250' rating is based on only the ratings of "regular voters". The exact number of votes a registered user would have to make to be considered to be a user who votes regularly has been kept secret. IMDb has stated that to maintain the effectiveness of the top 250 list they "deliberately do not disclose the criteria used for a person to be counted as a regular voter".[25] In addition to other weightings, the top 250 films are also based on a weighted rating formula referred to in actuarial science as a credibility formula.[26] This label arises because a statistic is taken to be more credible the greater the number of individual pieces of information; in this case from eligible users who submit ratings. Though the current formula is not disclosed, IMDb originally used the following formula to calculate their weighted rating:[27][28]
            
            W = (Rv+Cm) / (v+m)
            
            where:
            
            W = weighted rating
            R = average for the movie as a number from 0 to 10 (mean) = (Rating)
            v = number of votes for the movie = (votes)
            m = minimum votes required to be listed in the Top 250 
                (currently 25,000)
            C = the mean vote across the whole report (currently 7.0)
            
            The W in this formula is equivalent to a Bayesian posterior mean
            */
            
            // Number of votes for this lesson
            var v = votes.length;
            
            // Average stars for this lesson
            var R = votes.reduce(function(a, b) { return a+b; }) / v;
            
            // Minimum number of votes required to get a rating
            var m = 5;
            
            function ave(arr) {
                var sum = 0, j=arr.length;
                for (i=0; i<j; i++) {
                    sum += arr[i];
                }
                
                return sum/j;
            }
            
            var C = ave(CC.lessons.map(function(lesson) {
                return ave(lesson.stars);
            }));
            
            var stars = ((R * v) + (C * m)) / (v + m);
            
        for (i=0; i<5; i++) {
            str += "<a class='star' href='#vote_" + i + "_stars_for_" + entry + "' onclick='CC.castVote(" + i + ",\"" + entry + "\"); event.stopPropagation();'>" + CC.stars[ i<stars ? "black" : "white" ] + "</a>";
        }
        
        return str + (" (" + v + " votes)");
    },
    
    castVote : function(vote, entry) {
        var entry_num = entry.substr(9);
        CC.lessons[entry_num].stars.push(vote);
        CC.$("#" + entry + "_votes").innerHTML = CC.starrer(entry, CC.lessons[entry_num].stars);
    },
    
    // parseUri 1.2.2
    // (c) Steven Levithan <stevenlevithan.com>
    // MIT License
    // http://blog.stevenlevithan.com/archives/parseuri
    parseUri : function(str) {
        var	o = {
            strictMode: false,
            key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
            q:   {
                name:   "queryKey",
                parser: /(?:^|&)([^&=]*)=?([^&]*)/g
            },
            parser: {
                strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
            }
        };
        
        var m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
            uri = {},
            i   = 14;
    
        while (i--) uri[o.key[i]] = m[i] || "";
    
        uri[o.q.name] = {};
        uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
            if ($1) uri[o.q.name][$1] = $2;
        });
    
        return uri;
    },
        
    // Figure out if there are any tags in the URI
    writeTags : function() {

        var uri = CC.parseUri(document.location);
        //console.log(uri.queryKey);
        if (uri.queryKey.tags) {
            var tags = uri.queryKey.tags;
            if (tags.toLowerCase() !== "all") {
            
                var tags_arr =  tags.split(";")
                
                                // make the elements unique
                                // http://stackoverflow.com/questions/1960473/unique-values-in-an-array
                                // if ['foo', 'bar', 'foo', 'baz']
                                // → ['foo', 'bar', 'baz']
                                .filter(function(value, index, self) { 
                                    return self.indexOf(value) === index;
                                });
                                
                CC.$("#tags").innerHTML = "<ul><li>" + 
                
                    // → the big long div tag → rendered as a list
                    tags_arr.map(function(tag) {
                      return "<div class='mtag'>x <a href='#' onclick='CC.removeTag(\"" + tag + "\"); return false;'>" + tag + "</a></div>";
                    }).join("</li><li>") + 
                    
                    "</li></ul>";
                
                return tags_arr;
            }
            else {
                CC.$("#tags").innerHTML = "";
                return [];
            }
        }
        else {
            CC.$("#tags").innerHTML = "";
            return [];
        }
    },
    
    // write() takes an array of lessons (see CC.lessons for format) and 
    // writes them out to the browser along with their tags
    write : function(q) {
        if (typeof(q) === "undefined") {
            
            // Some error message
            
            // Set 'q' to all lessons
            q = CC.lessons;
        }
        
        // Write tags above the resources. This *only* takes place when there are tags in the URI, that is, when the user has asked for resources with certain tags
        var tags_arr = CC.writeTags();
        
        var i, j, id;

        // and a DL of all learning resources
        var str = "<dl>";
        
        for (i=0, j=q.length; i<j; i++) {
            
            // An id for each learning resource entry
            id = q[i].title.replace(/ +/g, '').substr(0, 9).toLowerCase() + '_' + i;
            
            str += "<dt class='entry closed' id='" + id + "'>" + 
            
                        // Display the title of the learning resource…
                        q[i].title + 
                        
                        // the star rating system, if the 'withStars' flag is true…
                        (CC.withStars 
                            ? " <span id='" + id + "_votes'>" +  CC.starrer(id, q[i].stars) + "</span>"  
                            : "") + 
                        
                        // the tags for each resource
                        "<ul class='tags'>" + 
                        
                        q[i].tags.map(function(el) {
                            
                            // Highlight the tags that have been focused on by the user
                            if (typeof(tags_arr) !== "undefined") {
                                if (tags_arr.indexOf(el) !== -1) {
                                    return "<li class='curr'>" + el + "</li>" 
                                }
                                else {
                                    return "<li><a href='" + CC.uri + "?tags=" + el + "' onclick='CC.addTag(\"" + el + "\"); event.stopPropagation(); return false;'>" + el + "</a></li>";
                                }
                            }
                            else {
                                return "<li><a href='" + CC.uri + "?tags=" + el + "' onclick='CC.addTag(\"" + el + "\"); event.stopPropagation(); return false;'>" + el + "</a></li>";
                            }
                            
                        }).join("\n") + 
                         
                        "</ul>" + 
                        
                    "</dt>" + 
                    "<dd class='off'>" + 
                        q[i].body + 
                        "<br><a href='" + q[i].uri + "' target='_blank'>go to the resource</a>" + 
                    "</dd>";
        }
        
        str += "</dl>";
        
        // Write the lessons
        CC.$('#lessons').innerHTML = str;
        
        // Write the count in the header
        CC.$('#count').innerHTML = (q.length == CC.lessons.length 
                ? "currently " 
                : "now ") + 
            "listing " + 
            q.length + 
            (q.length == 1 
                ? " item"
                : " items");
        
        // Now, we attach a click listener to each learning resource to 
        // toggle its visibility
        var list = CC.$(".entry");
        
        for (i=0, j=list.length; i<j; i++) {                
            list[i].addEventListener("click", function(e) {
                if (this.nextElementSibling.className === "on") {
                    this.nextElementSibling.className = "off";
                    this.className = 'entry closed';
                }
                else {
                    
                    // Hide all dd elements off
                    for (j in list) {
                        if (typeof(list[j]) === "object") {
                            list[j].nextElementSibling.className = "off";
                            list[j].className = 'entry closed';
                        }
                    }
                    
                    this.nextElementSibling.className = "on";
                    this.className = 'entry open';
                }
                
                history.pushState(null, this.id, "?id=" + this.id);
            });
        }
    },
    
    lessonsByTags : function(tag) {
        if (tag === "all") {
            CC.write(CC.lessons);
        }
        else {
            var tags = [];
            
            if (tag.indexOf(';') != -1) {
                tags = tag.split(";");
            }
            else {
                tags.push(tag);
            }
            
            var findByTags = function(t_arr, l_arr) {
                var lessons = [];
                var t = t_arr.shift();
                
                for (var i=0, j=l_arr.length; i<j; i++) {
                    if (l_arr[i].tags.indexOf(t) != -1) {
                        lessons.push(l_arr[i]);
                    }
                }
                
                if (t_arr.length > 0) {
                    findByTags(t_arr, lessons);
                }
                else {
                    CC.write(lessons);
                }
            }
            
            findByTags(tags, CC.lessons);
        }
    },
    
    /*
    Returns a function, that, as long as it continues to be 
    invoked, will not be triggered. The function will be called 
    after it stops being called for N milliseconds. If `immediate` 
    is passed, trigger the function on the leading edge, instead 
    of the trailing.
    */
    debounce: function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }
};